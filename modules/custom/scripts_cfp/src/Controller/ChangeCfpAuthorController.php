<?php

namespace Drupal\scripts_cfp\Controller;

/**
 * @file
 * Contains \Drupal\scripts_cfp\Controller\ChangeCfpAuthorController
 */

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides route responses for the accounts cration route.
 */
class ChangeCfpAuthorController {

    /**
     * Changes the authorship of cfp content.
     *
     * @return array
     *   A renderable array with results.
     */
    public function execute() {
        $adminCfpUser = user_load_by_name('AdminCFP403');
        $adminUser = user_load_by_name('ltacchini');
        $nids = \Drupal::entityQuery('node')
                    ->condition('type', 'cfp')
                    ->condition('uid', $adminUser->id())
                    ->execute();
        

        $ret = array(
            '#markup' => '<h3>Resultados</h3>',
        );

        $count = 0;
        $cfpContents = Node::loadMultiple($nids);
        $report = '';
        foreach ($cfpContents as $cfp) {
            $prevOwner = $cfp->getOwner();
            $cfp->setOwner($adminCfpUser);
            if ($cfp->save() == SAVED_UPDATED) {
                $count++;
                $report = $report . '<li>Se cambio el autor del CFP "' 
                        . $cfp->getTitle() 
                        . '" de "' 
                        . $prevOwner->getAccountName() 
                        . '" a "'
                        . $cfp->getOwner()->getAccountName()
                        . '"</li>';
            }
        }

        $ret['#markup'] = $ret['#markup'] 
                            . '<p>Cantidad de contenidos cambiados: ' 
                            . strval($count) 
                            . '</p>';

        $ret['#markup'] = $ret['#markup']
                            . '<ul>'
                            . $report
                            . '</ul>';
        
        return $ret;
    }


}
