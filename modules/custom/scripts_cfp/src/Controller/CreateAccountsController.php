<?php

namespace Drupal\scripts_cfp\Controller;

/**
 * @file
 * Contains \Drupal\scripts_cfp\Controller\CreateAccountsController
 */

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides route responses for the accounts cration route.
 */
class CreateAccountsController {

    /**
     * Creates the missing accounts and returns a page with the
     * results.
     *
     * @return array
     *   A renderable array with results of the accounts creation.
     */
    public function createAccounts() {
        $adminUser = user_load_by_name('AdminCFP403');
        $nids = \Drupal::entityQuery('node')
                    ->condition('type', 'estudiante')
                    ->condition('uid', $adminUser->id())
                    ->execute();
        

        $ret = array(
            '#markup' => '<h1>Creación de usurios</h1>',
        );
        
        //$createdAccounts = array();
        $countCreated = 0;
        $reportMessage = '';

        foreach ($nids as $nid) {
            $contentEstudiante = Node::load($nid);
            $names = trim($contentEstudiante->get('field_nombre')->value);
            $surnames = trim($contentEstudiante->get('field_apellidos')->value);
            $username = strtolower(explode(' ', $names)[0]) . '.' . strtolower(str_replace(' ', '', $surnames));
            $mail = $contentEstudiante->get('field_email')->value;
            $dni = $contentEstudiante->get('field_nro_de_documento')->value;
            $newAccount = $this->createUser($username, $dni, $mail);
            if ($newAccount/*!in_array($username, $createdAccounts)*/) {
                $contentEstudiante->setOwner($newAccount);
                $contentEstudiante->save();
                $countCreated = $countCreated + 1;
                //array_push($createdAccounts, $username);
            } else {
                \Drupal::messenger()->addWarning('La cuenta: ' . $username . ' no se pudo crear porque ya existe.');
            }
        }
        
        $countNotCreated = sizeof($nids) - $countCreated;
        $ret['#markup'] = $ret['#markup'] . 'Cantidad de usuarios creados: ' . strval($countCreated) . '<br>';
        $ret['#markup'] = $ret['#markup'] . 'Cantidad de usuarios no creados: ' . strval($countNotCreated) . '<br>';
        $ret['#markup'] = $ret['#markup'] . $reportMessage;
        return $ret;
    }

    private function createUser($username, $password, $email) {
        if (user_load_by_name($username)) {
            return null;
        }
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $user = User::create();
        $user->setPassword($password);
        $user->enforceIsNew();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->set("init", 'email');
        $user->set("langcode", $language);
        $user->set("preferred_langcode", $language);
        $user->set("preferred_admin_langcode", $language);
        $user->addRole('estudiante');
        $user->activate();
        $saved = $user->save();
        if ($saved == SAVED_NEW) {
            return $user;
        } else {
            return null;
        }
    }

}
