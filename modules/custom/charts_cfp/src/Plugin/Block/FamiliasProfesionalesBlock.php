<?php

namespace Drupal\charts_cfp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

const CURSOS_BLOCK_COLORS = [
  '#003F5C',
  '#58508D',
  '#BC5090',
  '#FF6361',
  '#FFA600',
  '#9BBFE0',
  '#E8A09A',
  '#FBE29F',
  '#C6D68F',
  '#000000',
  '#868686',
  '#C1C1C1',
  '#3D3D3D',
  '#7982B9',
  '#A5C1DC',
  '#E9F6FA',
  '#0674C4',
  '#DDDDDD',
  '#A9A9A9',
  '#C608D1',
  '#FF00FE',
  '#FF77FD',
  '#FFA9FD',
  '#2900A5',
];

/**
 * Provides a block with a chart
 * 
 * @Block(
 *  id = "familias_profesionales_block",
 *  admin_label = @Translation("Familias Profesionales Block"),
 * )
 */
class FamiliasProfesionalesBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

      $year = \Drupal::request()->query->get('fpYear');
      $results = $this->getData($year);
      if ($year == null) {
        $title = 'Preferencias históricas de cursos en estudiantes del CFP por familia profesional';
      } else {
        $title = 'Preferencias de estudiantes del CFP que cursaron en el año ' . $year . ' por familia profesional';
      }

      $options = [];
      $options['type'] = 'pie';
      $options['title'] = $title;
      // Google specific options...
      //$options['legend'] = 'none';

      // data format. not used by pie chart.
      $categories = [];

      $seriesData = array();
      $index = 0;
      foreach ($results as $record) {
        array_push($seriesData, [
          'name' => $record->familia_profesional,
          'data' => [intval($record->total)],
          'color' => CURSOS_BLOCK_COLORS[$index],
        ]);
        $index++;
      }

      /**
       * Cuando la base de datos responde con un solo grupo y $seriesData
       * queda con un solo array se construye mal el chart. Para que funcione
       * en todos los casos tiene que haber más de una serie. 
       * Agrego una serie vacía que nunca se muestra para que el chart 
       * siempre se renderice bien.
       */
      array_push($seriesData, [
        'name' => 'serie_vacia',
        'data' => [0],
        'color' => '#fff',
      ]);

      $build = [
        '#theme'      => 'familias_profesionales_view',
        '#library'    => 'google',
        '#categories' => $categories,
        '#seriesData' => $seriesData,
        '#options'    => $options, 
      ];

      return $build;
    }

    private function getData($year = null) {

      $cfpContentId = \Drupal::entityQuery('node')
                        ->condition('type', 'cfp')
                        ->condition('uid', \Drupal::currentUser()->id())
                        ->execute();

      $nid = null;
      foreach ($cfpContentId as $cfpId) {
        $nid = $cfpId;
      }

      $query = \Drupal::database()->select('node', 'n');

      $query->join('node__field_curso', 'table_curso', 'n.nid = table_curso.entity_id');
      $query->join('node__field_familia_profesional', 
                   'table_familia_profesional', 
                   'table_curso.field_curso_target_id = table_familia_profesional.entity_id');
      $query->join('node__field_nombre', 
                   'table_nombre', 
                   'table_familia_profesional.field_familia_profesional_target_id = table_nombre.entity_id');
      $query->join('node__field_apertura',
                   'table_aperturas',
                   'n.nid = table_aperturas.entity_id');
      $query->join('node__field_cfp',
                   'table_cfp',
                   'table_aperturas.field_apertura_target_id = table_cfp.entity_id');                   

      if ($year != null) {
        $query->join('node_field_data', 'table_data', 'n.nid = table_data.nid');
        $query->where('year(from_unixtime(table_data.created)) = :year_placeholder', 
                [':year_placeholder' => $year]);
      }

      $query->condition('n.type', 'asignacion');
      $query->condition('table_cfp.field_cfp_target_id', $nid);

      $count_expression = $query->addExpression('COUNT(n.nid)', 'total');
      $familia_profesional = $query->addField('table_nombre', 
                                              'field_nombre_value', 
                                              'familia_profesional');

      $query->groupBy($familia_profesional);

      return $query->execute();
    }

    /**
    * {@inheritdoc}
    * return 0 If you want to disable caching for this block.
    */
    public function getCacheMaxAge() {
        return 0;
    }
}