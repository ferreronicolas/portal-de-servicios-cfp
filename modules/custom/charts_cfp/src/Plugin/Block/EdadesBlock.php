<?php

namespace Drupal\charts_cfp\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a chart
 * 
 * @Block(
 *  id = "edades_block",
 *  admin_label = @Translation("Edades Block"),
 * )
 */
class EdadesBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

      $year = \Drupal::request()->query->get('edadesYear');

      if ($year == null) {
        $year = date('Y');
      }
      $results = $this->getDataByYear($year);
      $title = 'Distribución de edades en estudiantes del CFP que cursaron en el año ' . $year;

      $options = [];
      $options['type'] = 'column';
      $options['title'] = $title;
      $options['yaxis_title'] = $this->t('Candidad de estudiantes');
      $options['xaxis_title'] = $this->t('Edades');
      $options['yaxis_min'] = '';
      $options['yaxis_max'] = '';
      // Google specific options...
      $options['legend'] = 'none';

      // data format.
      $categories = [
        'Menor de 21 años',
        'Entre 21 y 30 años',
        'Entre 31 y 40 años',
        'Entre 41 y 50 años',
        'Entre 51 y 60 años',
        'Mayor de 60 años'
      ];

      $data = array_combine($categories, [0,0,0,0,0,0]);
      foreach ($results as $record) {
        $data[$record->rango_etareo] = intval($record->total);
      }

      $seriesData = [
        [
          'name' => 'Cantidad de estudiantes',
          'color' => '#0d233a',
          'data' => array_values($data),
        ],
      ];

      $build = [
        '#theme'      => 'edades_view',
        '#library'    => 'google',
        '#categories' => $categories,
        '#seriesData' => $seriesData,
        '#options'    => $options,
      ];

      return $build;
    }

    private function getDataByYear($year = NULL) {

      $cfpContentId = \Drupal::entityQuery('node')
                        ->condition('type', 'cfp')
                        ->condition('uid', \Drupal::currentUser()->id())
                        ->execute();

      $nid = null;
      foreach ($cfpContentId as $cfpId) {
        $nid = $cfpId;
      }

      $query = \Drupal::database()->select('node', 'n');
      
      $query->join('node_field_data',
                   'table_data',
                   'n.nid = table_data.nid');
      $query->join('node__field_estudiantes',
                   'table_estudiantes',
                   'n.nid = table_estudiantes.entity_id');
      $query->join('node__field_fecha_de_nacimiento',
                   'table_fechas',
                   'table_estudiantes.field_estudiantes_target_id = table_fechas.entity_id');
      $query->join('node__field_apertura',
                   'table_aperturas',
                   'n.nid = table_aperturas.entity_id');
      $query->join('node__field_cfp',
                   'table_cfp',
                   'table_aperturas.field_apertura_target_id = table_cfp.entity_id');

      $count_expression = $query->addExpression('COUNT(n.nid)', 'total');
      $group_expression = $query->addExpression("CASE
      WHEN :year_placeholder - year(field_fecha_de_nacimiento_value) < 21 THEN 'Menor de 21 años'
      WHEN :year_placeholder - year(field_fecha_de_nacimiento_value) <= 30 THEN 'Entre 21 y 30 años'
      WHEN :year_placeholder - year(field_fecha_de_nacimiento_value) <= 40 THEN 'Entre 31 y 40 años'
      WHEN :year_placeholder - year(field_fecha_de_nacimiento_value) <= 50 THEN 'Entre 41 y 50 años'
      WHEN :year_placeholder - year(field_fecha_de_nacimiento_value) <= 60 THEN 'Entre 51 y 60 años'
      ELSE 'Mayor de 60 años'
      END", 'rango_etareo', [':year_placeholder' => $year]);
        
      $query->where('year(from_unixtime(table_data.created)) = :year_placeholder', 
                    [':year_placeholder' => $year]);
      
      $query->condition('n.type', 'asignacion');
      $query->condition('table_cfp.field_cfp_target_id', $nid);

      $query->groupBy($group_expression);

      return $query->execute();
    }

    /**
    * {@inheritdoc}
    * return 0 If you want to disable caching for this block.
    */
    public function getCacheMaxAge() {
        return 0;
    }
}