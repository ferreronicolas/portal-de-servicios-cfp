<?php

namespace Drupal\charts_cfp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

const YEARS = [
  'Histórico', 
  '2018', 
  '2019',
  '2020',
  '2021', 
  '2022', 
  '2023',
  '2024',
  '2025',
  '2026',
];

/**
 * Implements a codimth Simple Form API.
 */
class ChartsForm extends FormBase {

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $edadesYearParam = \Drupal::request()->query->get('edadesYear');
    if ($edadesYearParam == null) {
      $edadesYearParam = date('Y');
    }

    $fpYearParam = \Drupal::request()->query->get('fpYear');

    // Item
    $form['description'] = [
      '#type' => 'item',
      '#markup' => 'Configuración de reportes',
    ];

    $form['edades_year'] = [
      '#type' => 'select',
      '#title' => 'Año para el reporte de edades',
      '#description' => 'Seleccione el año para mostrar en el reporte de edades',
      '#options' => array_combine(array_slice(YEARS, 1), array_slice(YEARS, 1)),
      '#default_value' => $edadesYearParam,
    ];

    $form['fp_year'] = [
      '#type' => 'select',
      '#title' => 'Año para el reporte de familias profesionales más elegidas',
      '#description' => 'Seleccione el año para mostrar en el reporte de familiar profesionales',
      '#options' => array_combine(YEARS, YEARS),
      '#default_value' => $fpYearParam,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Aplicar cambios',
    ];

    // Theme
    $form['#theme'] = 'reports_view';
    // styles
    $form['#attached']['library'][] = 'charts_cfp/charts-config-form';

    return $form;
  }

  /**
   * @return string
   */
  public function getFormId() {
    return 'charts_form';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $edadesYear = $form_state->getValue('edades_year');
    if(!in_array($edadesYear, array_slice(YEARS, 1))) {
      $form_state->setErrorByName('edades_year', 'No puede elegir un año que no esté en la lista.');
    }

    $fpYear = $form_state->getValue('fp_year');
    if (!in_array($fpYear, YEARS)) {
      $form_state->setErrorByName('fp_year', 'No puede elegir una opción que no esté en la lista.');
    }
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $param = array();
    
    $edadesYear = $form_state->getValue('edades_year');
    $params['edadesYear'] = $edadesYear;
    $fpYear = $form_state->getValue('fp_year');
    if ($fpYear != YEARS[0]) {
        $params['fpYear'] = $fpYear;
    }
    $form_state->setRedirect('charts_cfp.charts_dashboard', $params);
  }

}