<?php

namespace Drupal\formularios_cfp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

const REDIRECT_URL = '/aperturas';
/**
 * Implements a codimth Simple Form API.
 */
class FormAperturaAdmincfp extends FormBase {

	/**
	 * @param array $form
	 * @param FormStateInterface $form_state
	 * @return array
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		// get asignaciones en las que el estudiante aparece inscripto
		$nids = \Drupal::entityQuery('node')
			->condition('type', 'curso')
			->execute();

		$cursos = Node::loadMultiple($nids);

		$nids2 = \Drupal::entityQuery('node')
			->condition('type', 'docente')
			->execute();

		$docentes = Node::loadMultiple($nids2);

		$cursosForm = array_map(function ($v) {
			return $v->get('field_nombre')->value;
		}, $cursos);

		$docentesForm = array_map(function ($v) {
			return $v->get('field_apellidos')->value . ',  ' . $v->get('field_nombre')->value;
		}, $docentes);

		$form['curso'] = [
			'#type' => 'select',
			'#title' => 'Curso',
			'#description' => t('Seleccione uno de los cursos.'),
			'#options' => $cursosForm,
			'#required' => TRUE,
		];

		$form['docente'] = [
			'#type' => 'select',
			'#title' => 'Docente',
			'#description' => t('Seleccione uno de los docentes.'),
			'#options' => $docentesForm,
			'#required' => TRUE,
		];

		$form['turno'] = [
			'#type' => 'select',
			'#options' => ['Mañana' => $this->t('Mañana'), 'Tarde' => $this->t('Tarde'), 'Noche' => $this->t('Noche'), 'Intermedio' => $this->t('Intermedio'), 'Vespertino' => $this->t('Vespertino')],
			'#title' => 'Turno',
			'#description' => t('Seleccione el turno de la apertura.'),
			'#required' => TRUE,
		];

		$form['fecha_inicio'] = [
			'#type' => 'date',
			'#title' => 'Fecha Inicio',
			'#date_format' => 'y-m-d',
			'#required' => TRUE,
		];

		$form['fecha_fin'] = [
			'#type' => 'date',
			'#title' => 'Fecha Fin',
			'#date_format' => 'y-m-d',
			'#required' => TRUE,
		];

		$form['cantidad_vacantes'] = [
			'#type' => 'number',
			'#title' => 'Cantidad Vacantes',
			'#required' => TRUE,
		];

		$form['actions'] = [
			'#type' => 'actions',
		];

		// Add a submit button
		$form['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Aceptar'),
		];

/*
// Add a reset button
$form['actions']['reset'] = [
'#type' => 'button',
'#button_type' => 'reset',
'#value' => $this->t('Cancelar'),
'#attributes' => [
'onclick' => 'this.form.reset(); return false;',
],
];
 */

//    $form['#attached']['library'][] = 'formularios_cfp/seguimiento-form';

		return $form;
	}

	/**
	 * @return string
	 */
	public function getFormId() {
		return 'form_apertura_admincfp';
	}

	/**
	 * @param array $form
	 * @param FormStateInterface $form_state
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {

		$fechaInicio = $form_state->getValue('fecha_inicio');
		$fechaFin = $form_state->getValue('fecha_fin');

		$hoy = date("Y-m-d");

		if ($fechaInicio >= $fechaFin) {
			$form_state->setErrorByName('fecha_fin', 'La fecha de fin de la apertura debe ser mayor que la fecha de inicio.');
		}
		if ($fechaInicio < $hoy) {
			$form_state->setErrorByName('fecha_inicio', 'La fecha de inicio no puede ser anterior al día de hoy.');
		}

	}

	/**
	 * @param array $form
	 * @param FormStateInterface $form_state
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		$user = \Drupal::currentUser();
		$language = \Drupal::languageManager()->getCurrentLanguage()->getId();

		$nids = \Drupal::entityQuery('node')
			->condition('type', 'cfp')
			->condition('uid', $user->id())
			->execute();

		$cfps = Node::loadMultiple($nids);

		$cfp = array_map(function ($v) {
			return $v->get('field_nro_cfp')->value;
		}, $cfps);

		$idCfp;
		$nroCfp;
		foreach ($cfp as $k => $v) {
			$idCfp = $k;
			$nroCfp = $v;
			break;
		}

		$node = Node::create(array(
			'langcode' => $language,
			'type' => 'apertura',
			'title' => 'Apertura' . '_CFP_' . $nroCfp,
			'uid' => $user->id(),
			'status' => 1,
			'field_fecha_de_inicio' => $form_state->getValue('fecha_inicio'),
			'field_fecha_de_fin' => $form_state->getValue('fecha_fin'),
			'field_cantidad_vacantes' => $form_state->getValue('cantidad_vacantes'),
			'field_docente' => $form_state->getValue('docente'),
			'field_curso' => $form_state->getValue('curso'),
			'field_turno' => $form_state->getValue('turno')

			, 'field_cfp' => $idCfp,

		));
		$node->save();

		$this->messenger()->addStatus('Apertura guardada correctamente.');

		\Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', REDIRECT_URL);

	}

}
