<?php

namespace Drupal\formularios_cfp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;

const ENTITY_TYPE_APERTURA = 'apertura';
const ENTITY_TYPE_ASIGNACION = 'asignacion';

const REDIRECT_URL = '/materias-disponibles';
const REDIRECT_URL2 = '/materias-cursadas';

/**
 * Implements a codimth Simple Form API.
 */
class InscripcionForm extends FormBase {

	public function buildForm(array $form, FormStateInterface $form_state) {

		$storage = \Drupal::getContainer()->get('entity_type.manager')->getStorage('node');
		$nids = $storage->getQuery();

		$nids = $nids->condition('type', 'curso')
			->condition('status', 1)
			->condition('nid', \Drupal::request()->query->get('idCurso'))
			->sort('title')
			->execute();

		$cursos = $storage->loadMultiple($nids);
		$curso = $cursos[\Drupal::request()->query->get('idCurso')];

		$form['mensaje'] = [
			'#type' => 'item',
			'#markup' => '¿Está seguro que desea inscribirse en la materia correspondiente al curso de  ' . $curso->get('field_nombre')->value . ' ?',
		];

		// $form['usuario'] = [
		// 	'#type' => 'hidden',
		// 	'#value' => \Drupal::currentUser()->id(),
		// ];

		$form['apertura'] = [
			'#type' => 'hidden',
			'#value' => \Drupal::request()->query->get('idApertura'),
		];

		// CheckBoxes.
		$form['curso'] = [
			'#type' => 'hidden',
			'#value' => \Drupal::request()->query->get('idCurso'),
		];

		$form['estado'] = [
			'#type' => 'hidden',
			'#value' => 'en_curso',
		];

		$form['actions'] = [
			'#type' => 'actions',
		];

		// Add a submit button
		$form['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Inscribirme'),
		];

/*
// Add a reset button
$form['actions']['callback'] = [
'#type' => 'button',
'#button_type' => 'callback',
'#value' => $this->t('Volver'),
'#attributes' => [
'onclick' =>  "\Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', REDIRECT_URL); ",
//     'onclick' => 'this.form.reset(); return false;',
],
];
 */

		return $form;
	}

	public function getFormId() {
		return 'inscripcion_main_form';
	}

	public function validateForm(array &$form, FormStateInterface $form_state) {

		/* Validación de si ya está inscripcto en la materia */

		$idUser = \Drupal::currentUser()->id();
		$idApertura = \Drupal::request()->query->get('idApertura');
		$idCurso = \Drupal::request()->query->get('idCurso');

		$storage = \Drupal::getContainer()->get('entity_type.manager')->getStorage('node');
		$nids = $storage->getQuery();

		$nids = $nids->condition('type', ENTITY_TYPE_ASIGNACION)
			->condition('status', 1)
			->condition('field_alumno', $idUser)
			->condition('field_apertura', $idApertura)
			->sort('title')
			->execute();
/*
$aperturas = [];
$estudiantes = [];
 */

		$error = 0;

		$nodes = $storage->loadMultiple($nids);

		$registros = count($nodes);

		if ($registros > 0) {
			$error++;
			$form_state->setErrorByName('mensaje', 'Usted ya se encuentra inscripto en esta materia.');
		}
/*
$aperturas = [];
$estudiantes = [];
foreach($nodes as $node){
$apId = $node->get('field_apertura')->target_id;
$apId = intval($apId);

$aperturas[$apId] = $apId;

if($apId == $idApertura){
$est = $node->get('field_estudiantes')->target_id;
$est = intval($est);
$estudiantes[$est] = $est;
}
}

$res = false;
$res = in_array($idUser, $estudiantes);

if( $res  ){
$form_state->setErrorByName('mensaje', 'Usted ya se encuentra inscripto en esta materia.');
}
 */

		/* Validación del cupo  */
		if ($error == 0) {
			$nids = \Drupal::entityQuery('node')
				->condition('type', ENTITY_TYPE_APERTURA)
				->condition('nid', \Drupal::request()->query->get('idApertura'))
				->execute();

			$aper = Node::loadMultiple($nids);

			$cupo = array_map(function ($v) {
				return $v->get('field_cantidad_vacantes')->value;
			}, $aper);

			$nids = \Drupal::entityQuery('node')
				->condition('type', ENTITY_TYPE_ASIGNACION)
				->condition('field_apertura', \Drupal::request()->query->get('idApertura'))
				->execute();

			$asig = Node::loadMultiple($nids);

			$anotados = array_map(function ($v) {
				return $v->get('field_apertura')->target_id;
			}, $asig);

			$vacantes = $cupo[$idApertura];
			$inscriptos = count($anotados);

			if ($inscriptos >= $vacantes) {
				$form_state->setValue('estado', 'en_espera');

				//	$form_state->setErrorByName('mensaje', json_encode($arr)   );
				$this->messenger()->addStatus('Esta materia ha alcanzado el cupo de vacantes. Usted quedará anotado en la lista de espera.');
			}
		}
	}

	public function submitForm(array &$form, FormStateInterface $form_state) {

		$user = \Drupal::currentUser();
		// return [vid => nid]
		$nids = \Drupal::entityQuery('node')
					->condition('type', 'estudiante')
					->condition('uid', $user->id())
					->execute();

		$contentId = null;
		foreach ($nids as $id) {
			$contentId = $id;
		}

		$language = \Drupal::languageManager()->getCurrentLanguage()->getId();
		$node = \Drupal\node\Entity\Node::create(array(
			'type' => 'asignacion',
			'langcode' => $language,
			'uid' => $user->id(),
			'status' => 1,

			'field_apertura' => $form_state->getValue('apertura'),
			'field_curso' => $form_state->getValue('curso'),
			'field_estudiantes' => $contentId,
			'field_estado' => $form_state->getValue('estado'),
			'field_alumno' => $user->id(),
		));

		$node->save();

		$this->messenger()->addStatus('Inscripción guardada correctamente.');

		\Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', REDIRECT_URL2);

	}

}
