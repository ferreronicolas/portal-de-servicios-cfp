<?php

namespace Drupal\formularios_cfp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

const REDIRECT_URL = '/user';
/**
 * Implements a codimth Simple Form API.
 */
class SeguimientoForm extends FormBase {

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // returns array [$vid => $nid]
    $estudianteContentId = \Drupal::entityQuery('node')
                            ->condition('type', 'estudiante')
                            ->condition('uid', \Drupal::currentUser()->id())
                            ->execute();

    // get asignaciones en las que el estudiante aparece inscripto
    $nids = \Drupal::entityQuery('node')
              ->condition('type', 'asignacion')
              ->condition('field_estudiantes', array_values($estudianteContentId))
              // condition with dates
              ->execute();
    
    $asignacionesCursadas = Node::loadMultiple($nids);
    $cursosCursados = array();
    foreach($asignacionesCursadas as $asignacion) {
      // get id del curso asociado a la asignacion
      $cId = $asignacion->get('field_curso')->getValue()[0]['target_id'];
      // get nombre del curso asociado a la asignacion
      $cName = Node::load($cId)->get('field_nombre')->value;
      $cursosCursados[$cId] = $cName;
    }

    // // Item
    // $form['description'] = [
    //   '#type' => 'item',
    //   '#markup' => 'Seguimiento para: ' . \Drupal::currentUser()->getAccountName(),
    // ];

    $form['curso_realizado'] = [
      '#type' => 'select',
      '#title' => 'Curso',
      '#description' => t('Seleccione uno de los cursos realizados para hacer un seguimiento.'),
      '#options' => $cursosCursados,
    ];

    // CheckBoxes.
    $form['situacion_laboral'] = [
      '#type' => 'radios',
      '#options' => ['Si' => $this->t('Si'), 'No' => $this->t('No')],
      '#title' => $this->t('¿Trabaja actualmente?:'),
      '#description' => t('Seleccione si trabaja en un puesto relacionado al curso realizado.'),
    ];


    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enviar'),
    ];

    // Add a reset button
    $form['actions']['reset'] = [
      '#type' => 'button',
      '#button_type' => 'reset',
      '#value' => $this->t('Cancelar'),
      '#attributes' => [
        'onclick' => 'this.form.reset(); return false;',
      ],
    ];

    $form['#attached']['library'][] = 'formularios_cfp/seguimiento-form';

    return $form;
  }

  /**
   * @return string
   */
  public function getFormId() {
    return 'seguimiento_form';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $situacion_laboral = $form_state->getValue('situacion_laboral');
    if(!$situacion_laboral) {
      $form_state->setErrorByName('situacion_laboral', 'Debe elegir una opción para situación laboral.');
    }
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();

    $nids = \Drupal::entityQuery('node')
			->condition('type', 'estudiante')
			->condition('uid', $user->id())
			->execute();

		$estudiantes = Node::loadMultiple($nids);
    $estudianteContent;
		foreach ($estudiantes as $content) {
      $estudianteContent = $content;
    };

    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $situacion_laboral = $form_state->getValue('situacion_laboral');
    $curso_elegido = $form_state->getValue('curso_realizado');

    $node = Node::create(array(
      'langcode' => $language,
      'type'  => 'seguimiento',
      'title' => 'seguimiento' . '_' . $user->getAccountName() . date('Y-m-d-h-i-s'),
      'uid' => $user->id(),
      'status' => 1,
      'field_fecha' => date('Y-m-d'), // fecha actual
      'field__trabaja_actualmente_' => ($situacion_laboral === 'Si' ? 1 : 0),
      'field_estudiantes' => array($estudianteContent->id()),
      'field_curso' => array($curso_elegido)
    ));
    $node->save();
    
    $this->messenger()->addStatus('Seguimiento guardado correctamente.');
    $this->messenger()->addStatus($this->t('Su situación laboral es: @situacion_laboral.',
            ['@situacion_laboral' => ($situacion_laboral === 'Si' ? '"Trabaja Actualmente"' : '"No trabaja actualmente')]));
    $this->messenger()->addStatus('Curso elegido: ' . $curso_elegido);

    \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', REDIRECT_URL);
  }

}