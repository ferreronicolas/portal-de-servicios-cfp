<?php

/**
 * @file
 * Provides functionality to disallow a self registered user (estudiante) to
 * create their personal data more than one time.
 */

use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;

const CONTENT_TYPE_ESTUDIANTE = 'estudiante';
const CONTENT_TYPE_CFP = 'cfp';
const ESTUDIANTE_ROLE = 'estudiante';
const ADMINCFP_ROLE = 'admincfp';
const REDIRECT_URL_ESTUDIANTE = '/node/add/estudiante';
const REDIRECT_URL_ADMINCFP = '/node/add/cfp';
const REDIRECT_MESSAGE_ESTUDIANTE = 'Para continuar complete su información personal.';
const REDIRECT_MESSAGE_ADMINCFP = 'Para continuar complete la información del CFP.';

/**
 * Implements hook_help().
 */
function redirect_login_help($route_name, RouteMatchInterface $route_match) {
  $output = "<h1>Redirect Login</h1>";
  switch ($route_name) {
    case 'help.page.redirect_login':
      $output .= "Este módulo provee la siguiente funcionalidad:";
      $output .= "<ul>";
      $output .= "<li>Cuando una cuenta con rol 'estudiante' inicia sesión 
                  y aún no creó un contenido de tipo 'estudiante' lo redirige 
                  a /node/add/estudiante</li>";
      $output .= "<li>Impide que una cuenta con rol 'estudiante' que no haya creado
                  un contenido de tipo 'estudiante' cree cualquier otro tipo de contenido.
                  El estudiante puede crear contenido solo cuando ya haya creado un contenido
                  de tipo 'estudiante' previamente.</li>";
      $output .= "<li>Impide que una cuenta con rol 'estudiante' cree más de un contenido de
                  tipo 'estudiante'.</li>";
      $output .= "<li>Cuando una cuenta con rol 'admincfp' inicia sesión 
                  y aún no creó un contenido de tipo 'cfp' lo redirige 
                  a /node/add/cfp</li>";
      $output .= "<li>Impide que una cuenta con rol 'admincfp' que no haya creado
                  un contenido de tipo 'cfp' cree cualquier otro tipo de contenido.
                  El admin cfp puede crear contenido solo cuando ya haya creado un contenido
                  de tipo 'cfp' previamente.</li>";
      $output .= "<li>Impide que una cuenta con rol 'admincfp' cree más de un contenido de
                  tipo 'cfp'.</li>";
      $output .= "</ul>";
      return $output;
  }
}

/**
 * Checks if the account is the author of at least one node of the
 * specified type.
 * 
 * @param account
 *  Which account will be used to check.
 * @param contentType
 *  The content type.
 * @return bool
 *  True if the account already has a node. False otherwise.
 */
function redirect_login_has_associated_content($account, $contentType): bool {
  // Query
  $query = \Drupal::entityQuery('node');
  $count = $query->condition('type', $contentType)
                ->condition('uid.entity.name', $account->getAccountName())
                ->count()
                ->execute();
  return $count >= 1;
}

/**
 * Implements hook_user_login()
 * 
 * If the user account never completed their personal information it
 * redirects the request to the node creation form.
 * 
 * @param account
 *  Which account is logging in.
 */
function redirect_login_user_login(UserInterface $account) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name == 'user.reset.login') {
    return;
  }

  $roles = $account->getRoles();
  if (in_array(ESTUDIANTE_ROLE, $roles)) {
    $message = REDIRECT_MESSAGE_ESTUDIANTE;
    $url = REDIRECT_URL_ESTUDIANTE;
    $type = CONTENT_TYPE_ESTUDIANTE;
  } elseif (in_array(ADMINCFP_ROLE, $roles)) {
    $message = REDIRECT_MESSAGE_ADMINCFP;
    $url = REDIRECT_URL_ADMINCFP;
    $type = CONTENT_TYPE_CFP;
  } else {
    return;
  }
  
  if (!redirect_login_has_associated_content($account, $type)) {
    \Drupal::messenger()->addMessage($message);
    $destination = Url::fromUserInput($url)->toString();
    \Drupal::service('request_stack')->getCurrentRequest()->query->set('destination', $destination);
  }
}

/**
 * Implements hook_ENTITY_TYPE_create_access().
 * 
 * If the account already has a node associated as author it fobids the request.
 * 
 * @param account
 *  Which account is trying to create a new node.
 * @param context
 *  An associative array of additional context values.
 * @param entity_bundle
 *  The entity bundle name.
 */
function redirect_login_node_create_access(AccountInterface $account, array $context, $entity_bundle) {
  $roles = $account->getRoles();
  if (in_array(ESTUDIANTE_ROLE, $roles)) {
    // la cuenta es estudiante
    if (redirect_login_has_associated_content($account, CONTENT_TYPE_ESTUDIANTE)) {
      // la cuenta ya creo su información personal
      if ($entity_bundle == CONTENT_TYPE_ESTUDIANTE) {
        // la cuenta intenta crear su información otra vez
        return AccessResult::forbidden();
      } else {
        return AccessResult::neutral();
      }
    } else {
      // la cuenta no creo su información personal
      if ($entity_bundle == CONTENT_TYPE_ESTUDIANTE) {
        // la cuenta está por crear su información personal
        return AccessResult::neutral();
      } else {
        // la cuenta intenta crear otro tipo de contenido antes de su información
        return AccessResult::forbidden();
      }
    }
  } elseif (in_array(ADMINCFP_ROLE, $roles)) {
    // la cuenta es admin cfp
    if (redirect_login_has_associated_content($account, CONTENT_TYPE_CFP)) {
      // la cuenta ya completó la información de su CFP
      if ($entity_bundle == CONTENT_TYPE_CFP) {
        // la cuenta intenta crear la información del CFP otra vez
        return AccessResult::forbidden();
      } else {
        return AccessResult::neutral();
      }
    } else {
      // la cuenta no completó la información de su CFP
      if ($entity_bundle == CONTENT_TYPE_CFP) {
        // la cuenta está por crear la información de su CFP
        return AccessResult::neutral();
      } else {
        // la cuenta intenta crear otro tipo de contenido antes de la información del CFP
        return AccessResult::forbidden();
      }
    }
  } else {
    // la cuenta no es estudiante ni admin CFP
    return AccessResult::neutral();
  }
}