<?php

namespace Drupal\Tests\redirect_login\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use \Drupal\Component\Render\FormattableMarkup;

/**
 * Hooks tests
 * 
 * @group redirect_login
 */
class HooksTest extends BrowserTestBase {
    
    /**
     * {@inheritdoc}
     */
    protected static $modules = ['node', 'redirect_login'];

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void {
        parent::setUp();
        // crear el tipo de contenido: estudiante (no necesito los fields)
        $this->drupalCreateContentType([
            'type' => 'estudiante',
        ]);
        // crear el tipo de contenido: cfp
        $this->drupalCreateContentType([
            'type' => 'cfp',
        ]);
    }

    private function createRoleEstudiante() {
        // crear el rol estudiante
        return $this->drupalCreateRole([
            'create estudiante content',
            'edit own estudiante content',
        ], 'estudiante', 'Estudiante');
    }

    private function createRoleAdminCfp() {
        // crear el rol AdminCFP
        return $this->drupalCreateRole([
            'create cfp content',
            'edit own cfp content',
        ], 'admincfp', 'AdminCFP');
    }

    private function createTestAccount($rid = NULL) {
        $pass = \Drupal::service('password_generator')->generate();
        $values = [
            'name' => 'nombre',
            'mail' => 'nombre@example.com',
            'pass' => $pass,
            'status' => 1,
        ];

        if (isset($rid)) {
            $values += [
                'roles' => [
                    $rid,
                ],
            ];
        }

        $account = User::create($values);
        $account->save();

        $valid_user = $account->id() !== NULL;
        $this
            ->assertTrue($valid_user, new FormattableMarkup('User created with name %name and pass %pass', [
            '%name' => 'nombre',
            '%pass' => $pass,
        ]), 'User login');
        if (!$valid_user) {
            return FALSE;
        }

        // Add the raw password so that we can log in as this user.
        $account->pass_raw = $pass;

        // Support BrowserTestBase as well.
        $account->passRaw = $account->pass_raw;

        return $account;
    }

    function testLoginAdmin() {
        // esperado: no haya redirect
        $this->drupalLogin($this->rootUser);
        $this->assertSession()->addressNotEquals('/node/add/estudiante');
    }

    function testLoginEstudianteSinContenidoAsociado() {
        // esperado: hay login, y se redirecciona a la pantalla /node/add/estudiante
        $estudianteRid = $this->createRoleEstudiante();
        $account = $this->createTestAccount($estudianteRid);
        $this->drupalLogin($account);
        $this->assertSession()->addressEquals('/node/add/estudiante');
    }

    function testLoginAdminCFPSinContenidoAsociado() {
        // esperado: hay login, se redirecciona a la pantalla /node/add/cfp
        $adminCFPRid = $this->createRoleAdminCfp();
        $account = $this->createTestAccount($adminCFPRid);
        $this->drupalLogin($account);
        $this->assertSession()->addressEquals('/node/add/cfp');
    }

    function testLoginEstudianteConContenidoAsociado() {
        // esperado: no hay redireccion
        $estudianteRid = $this->createRoleEstudiante();
        $account = $this->createTestAccount($estudianteRid);
        $this->drupalLogin($account);
        // creo un contenido para el usuario. El author es el usuario que tiene
        // sesión iniciada por defecto.
        $this->drupalCreateNode(array(
            'type' => 'estudiante',
            'title' => 'estudiante_1',
        ));

        $this->drupalLogout($account);
        $this->drupalLogin($account);

        $this->assertSession()->addressNotEquals('/node/add/estudiante');
    }

    function testLoginAdminCFPConContenidoAsociado() {
        // esperado: no hay redireccion
        $adminCFPRid = $this->createRoleAdminCfp();
        $account = $this->createTestAccount($adminCFPRid);
        $this->drupalLogin($account);
        // creo un contenido para el usuario.
        $this->drupalCreateNode(array(
            'type' => 'cfp',
            'title' => 'cfp_1',
        ));

        $this->drupalLogout($account);
        $this->drupalLogin($account);

        $this->assertSession()->addressNotEquals('/node/add/cfp');
    }

    function testIntentarCrearContenidoEstudianteCuandoYaHayContenidoAsociado() {
        // esperado: no se permite crear contenido de tipo estudiante si ya se
        // creo uno previamente
        $estudianteRid = $this->createRoleEstudiante();
        $account = $this->createTestAccount($estudianteRid);
        $this->drupalLogin($account);
        // creo un contenido para el usuario. El author es el usuario que tiene
        // sesión iniciada por defecto.
        $this->drupalCreateNode(array(
            'type' => 'estudiante',
            'title' => 'estudiante_1',
        ));

        $this->drupalGet('/node/add/estudiante');
        // 403 forbidden
        $this->assertSession()->statusCodeEquals(403);
    }

    function testIntentarCrearContenidoCFPCuandoYaHayContenidoAsociado() {
        $adminCFPRid = $this->createRoleAdminCfp();
        $account = $this->createTestAccount($adminCFPRid);
        $this->drupalLogin($account);
        // creo un contenido para el usuario
        $this->drupalCreateNode(array(
            'type' => 'cfp',
            'title' => 'cfp_1',
        ));

        $this->drupalGet('/node/add/cfp');
        // 403 forbidden
        $this->assertSession()->statusCodeEquals(403);
    }
}